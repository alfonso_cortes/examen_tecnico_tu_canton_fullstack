# prueba-tecnica-vue

## Deeveloper Engineer
```
    I.S.C. Alfonso S. Cortes E.
    iscdokan@live.com
    452 107 1033
    Uruapan, Mich. Mx.
    05/Octubre/2021

```
## Software Factory
```
 Prueba Técnica Frontend VUE /SPA
 Para: Tu Cantón

```

## Rquereimientos
```

node    16.10.0
npm:    7.24.0
vue/cli 4.5.13 o posterior 

```

#-- Instalar NVM para multiples ambientes NODE
```
https://ricardogeek.com/como-instalar-varias-versiones-de-node-js-en-una-pc-con-linux/

Instalar Ambiente node con nvm
------------------------------
nvm install v16.10.0
nvm use v16.10.0

```

## Instalar dependencias del proyecto
```
npm install
```

### Compilar y Arrancar entorno de Desarrollo
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### API REST
```
    ENDPOINTS IMPLEMENTADOS

    http://104.254.247.243:8000/api/pass/
    http://104.254.247.243:8000/api/logout/

    http://104.254.247.243:8000/api/clients/
    http://104.254.247.243:8000/api/client/1/

    http://104.254.247.243:8000/api/users/
    http://104.254.247.243:8000/api/user/1/

    http://104.254.247.243:8000/api/teams/

    http://104.254.247.243:8000/api/sales/team/1/
    http://104.254.247.243:8000/api/sales/user/1/
    http://104.254.247.243:8000/api/sales/client/1/

```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
