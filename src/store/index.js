import { createStore } from 'vuex'
import router from '../router'

export default createStore({
  state: {
    token: null,
    errorLogin: null,
    welcome: null,

    showTableU: false,
    showTableC: false,
    showTableT: false,
    Users: [],
    Clients: [],
    Teams: [],
    tableSalesC:[],
    tableSalesU: [],
    tableSalesT: [],
    
  },
  
  mutations: {
    setToken(state, payload){
      state.token = payload
    },

    setWelcome(state, payload){
      // -- Bienvenida
      state.welcome = payload
    },
    setError(state, payload){
      // -- Envio de Errores
      state.errorLogin = payload
    },setInfo(state, payload){
      // -- Envio de notificaciones
      state.infoForm = payload
    },

    setUsers(state, payload){
      // -- Obtiene usuarios
      state.Users= payload
    },
    setClients(state, payload){
      // -- Obtiene clientes
      state.Clients= payload
    },
    setTeams(state, payload){
      // -- Obtiene Equipos
      state.Teams= payload
    },
    setTableSalesUser(state, payload){
      // -- Obtiene ventas por usuario
      state.tableSalesU= payload
    },
    setTableSalesTeam(state, payload){
      // -- Obtiene ventas por equipo
      state.tableSalesT= payload
    },
    setTableSalesCli(state, payload){
      // -- Obtiene ventas por cliente
      state.tableSalesC= payload
    },
    setShowTableU(state, payload){
      // -- Muestra tabla Usuario
      state.showTableU = payload
    },
    setShowTableT(state, payload){
      // -- Muestra tabla Usuario
      state.showTableT = payload
    },
    setShowTableC(state, payload){
      // -- Muestra tabla Cliente
      state.showTableC = payload
    },
  },

  actions: {
    async loginTuCanton({commit}, creds){
      console.log('Credenciales: '+creds)
      try {
        const res = await fetch('http://104.254.247.243:8000/api/login/'+creds.usuario+'/'+creds.password+'/',{
          method: 'GET',
        })

        const respuesta = await res.json()
        console.log('Respuesta: ', respuesta.message)
        if (respuesta.code == 401) {
          // -- Fallo de login
          commit('setError', respuesta.message)
        } else {
          // -- Login exitoso
          commit('setWelcome', respuesta.message)
          commit('setToken', respuesta.data)
          localStorage.setItem('token', respuesta.data)
          // -- Envia a home
          router.push('/')
        }
    
      } catch(error) {
        console.log('Error: '+error)
        //commit('setError', respuesta.data.token)
      }

    },
    leerToken({commit}){
      if(localStorage.getItem('token')){
        commit('setToken', localStorage.getItem('token'))

      }else{
        commit('setToken', null)
      }
    },
    salir({ commit }) {
      localStorage.removeItem('token')
      commit('setToken', null)
      commit('setShowForm', false)
      commit('setWelcome', null)
      router.push('/login')
    },
    async getUsers({commit}){
      try {
        const res = await fetch('http://104.254.247.243:8000/api/users/', {
          method: 'GET',
        }) 
        let dataUsers = await res.json()
        commit('setUsers', dataUsers.data)   
      } catch (error) {
        console.log(error)    
      }
    },
    async getClients({commit}){
      try {
        const res = await fetch('http://104.254.247.243:8000/api/clients/', {
          method: 'GET',
        }) 
        let dataClients = await res.json()
        commit('setClients', dataClients.data)   
      } catch (error) {
        console.log(error)    
      }
    },
    async getTeams({commit}){
      try {
        const res = await fetch('http://104.254.247.243:8000/api/teams/', {
          method: 'GET',
        }) 
        let dataTeams = await res.json()
        commit('setTeams', dataTeams.data)   
      } catch (error) {
        console.log(error)    
      }
    },
    async getSalesByUser({commit},usr){
      try {
        const res = await fetch('http://104.254.247.243:8000/api/sales/user/'+usr+'/', {
          method: 'GET',
        }) 
        let salesUser = await res.json()
        commit('setTableSalesUser', salesUser.data)   
      } catch (error) {
        console.log(error)    
      }
    },
    async getSalesByTeam({commit},team){
      try {
        const res = await fetch('http://104.254.247.243:8000/api/sales/team/'+team+'/', {
          method: 'GET',
        }) 
        let salesTeam = await res.json()
        commit('setTableSalesTeam', salesTeam.data)   
      } catch (error) {
        console.log(error)    
      }
    },
    async getSalesByCli({commit},cli){
      try {
        const res = await fetch('http://104.254.247.243:8000/api/sales/client/'+cli+'/', {
          method: 'GET',
        }) 
        let salesCli = await res.json()
        commit('setTableSalesCli', salesCli.data)   
      } catch (error) {
        console.log(error)    
      }
    },
    ShowTableU({commit}, arg){
      commit('setShowTableU', arg)
    },
    ShowTableT({commit}, arg){
      commit('setShowTableT', arg)
    },
    ShowTableC({commit}, arg){
      commit('setShowTableC', arg)
    },

  },
  getters: {
    authProtect(state) {
      // -- Verifica autenticación
      return !!state.token
    },
    statusForm(state){
      // -- Verifica estado de formulario
      return state.showForm
    },
    welcome(state){
      // -- Mensaje de Bienvenida
      return state.welcome
    },
  },

  modules: {

  }
})
