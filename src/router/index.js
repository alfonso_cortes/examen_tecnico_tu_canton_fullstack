import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {authProtect: true}
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  },
 
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  const IsAuthProtect = to.matched.some(item => item.meta.authProtect)
  // -- Verifica proteccion de ruta privada
  if(IsAuthProtect){
    // -- Verifica proteccion de autenticacion
    if(store.getters.authProtect){
      next()
    } else {
      next('/login')
    }  
  } else {
    next()
  }
})

export default router
